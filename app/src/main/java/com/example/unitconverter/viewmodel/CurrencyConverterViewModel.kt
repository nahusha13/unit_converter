package com.example.unitconverter.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CurrencyConverterViewModel:ViewModel() {
    var integerUserInput = MutableLiveData<String>("1")
    var spinnerInput = MutableLiveData<String>("mm")

    var counter = MutableLiveData<Int>(1)

    var userInputInRupees = MutableLiveData<Double>(0.0)

    var rupessTextView = MutableLiveData<Double>()
    var dolloarTextView = MutableLiveData<Double>()
    var poundsTextView = MutableLiveData<Double>()
    var dinarTextView = MutableLiveData<Double>(10.0)

}