package com.example.unitconverter.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DistanceConverterViewModel:ViewModel() {
    var integerUserInput = MutableLiveData<String>("1")
    var spinnerInput = MutableLiveData<String>("mm")

    var counter = MutableLiveData<Int>(1)

    var userInputInMetres = MutableLiveData<Double>(0.0)

    var metreTextView = MutableLiveData<Double>()
    var kilometreTextView = MutableLiveData<Double>()
    var mileTextView = MutableLiveData<Double>()
}