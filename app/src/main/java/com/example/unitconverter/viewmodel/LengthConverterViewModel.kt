package com.example.unitconverter.viewmodel

import android.view.animation.Transformation
import androidx.lifecycle.*

class LengthConverterViewModel:ViewModel() {

    var integerUserInput = MutableLiveData<String>("1")
    var spinnerInput = MutableLiveData<String>("mm")

    var counter = MutableLiveData<Int>(1)

    var userInputInMM = MutableLiveData<Double>(0.0)

    var mmTextView = MutableLiveData<Double>()
    var cmTextView = MutableLiveData<Double>()
    var mTextView = MutableLiveData<Double>()
    var kmTextView = MutableLiveData<Double>()
}

