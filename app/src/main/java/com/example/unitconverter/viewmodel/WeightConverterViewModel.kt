package com.example.unitconverter.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class WeightConverterViewModel:ViewModel() {
    var integerUserInput = MutableLiveData<String>("1")
    var spinnerInput = MutableLiveData<String>("mm")

    var counter = MutableLiveData<Int>(1)

    var userInputInGm = MutableLiveData<Double>(0.0)

    var gmTextView = MutableLiveData<Double>()
    var kgTextView = MutableLiveData<Double>()
    var lbsTextView = MutableLiveData<Double>()
    var tonneTextView = MutableLiveData<Double>(10.0)
}