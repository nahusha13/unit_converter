package com.example.unitconverter.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TemperatureConverterViewModel:ViewModel() {

    var integerUserInput = MutableLiveData<String>("0")
    var spinnerInput = MutableLiveData<String>("c")

    var counter = MutableLiveData<Int>(1)

    var userInputInCelsius = MutableLiveData<Double>(0.0)   

    var celsiusTextView = MutableLiveData<Double>()
    var FahrenheitTextView = MutableLiveData<Double>()

}