package com.example.unitconverter.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.unitconverter.R
import com.example.unitconverter.databinding.ActivityWeightConverterBinding
import com.example.unitconverter.viewmodel.WeightConverterViewModel

class WeightConverter : AppCompatActivity() {
    lateinit var binding:ActivityWeightConverterBinding
    lateinit var weightConverterVM: WeightConverterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_UnitConverter)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_weight_converter)
        weightConverterVM = ViewModelProvider(this).get(WeightConverterViewModel::class.java)
        binding.weightVm =weightConverterVM
        binding.lifecycleOwner = this

        binding.spinnerUnitSelector.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var text:String = binding.spinnerUnitSelector.selectedItem.toString()
                weightConverterVM.spinnerInput.value = text

                weightConverterVM.counter.value = weightConverterVM.counter.value?.plus(1)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
        weightConverterVM.integerUserInput.observe(this, Observer {
            if(!binding.etUserInput.text.isNullOrEmpty()) {
                weightConverterVM.counter.value = weightConverterVM.counter.value?.plus(1)
            }
        })

        weightConverterVM.counter.observe(this, Observer {
            if (!binding.etUserInput.text.isNullOrEmpty()) {
                if(weightConverterVM.spinnerInput.value.toString() == "gm") {
                    weightConverterVM.userInputInGm.value = weightConverterVM.integerUserInput.value?.toDouble()
                }else if(weightConverterVM.spinnerInput.value.toString() == "kg") {
                    weightConverterVM.userInputInGm.value = weightConverterVM.integerUserInput.value?.toDouble()?.times(1000)
                }else if(weightConverterVM.spinnerInput.value.toString() == "pounds") {
                    weightConverterVM.userInputInGm.value = weightConverterVM.integerUserInput.value?.toDouble()?.times(453.592)
                }else if(weightConverterVM.spinnerInput.value.toString() == "tonne") {
                    weightConverterVM.userInputInGm.value = weightConverterVM.integerUserInput.value?.toDouble()?.times(Math.pow(10.0,6.0))
                }
            }
        })

        weightConverterVM.userInputInGm.observe(this, Observer {
            weightConverterVM.gmTextView.value = it.toDouble()
            weightConverterVM.kgTextView.value = it.toDouble()/1000.0
            weightConverterVM.lbsTextView.value = it.toDouble()/453.592
            weightConverterVM.tonneTextView.value = it.toDouble()/(Math.pow(10.0,6.0))
        })
    }
}