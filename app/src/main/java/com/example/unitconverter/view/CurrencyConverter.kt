package com.example.unitconverter.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.unitconverter.R
import com.example.unitconverter.databinding.ActivityCurrencyConverterBinding
import com.example.unitconverter.viewmodel.CurrencyConverterViewModel
import com.example.unitconverter.viewmodel.WeightConverterViewModel
import java.util.*
import kotlin.math.roundToInt

class CurrencyConverter : AppCompatActivity() {
    lateinit var binding:ActivityCurrencyConverterBinding
    lateinit var currencyConverterVM:CurrencyConverterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_currency_converter)
        currencyConverterVM = ViewModelProvider(this).get(CurrencyConverterViewModel::class.java)
        binding.currencyVM = currencyConverterVM
        binding.lifecycleOwner = this

        binding.spinnerUnitSelector.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var text:String = binding.spinnerUnitSelector.selectedItem.toString()
                currencyConverterVM.spinnerInput.value = text

                currencyConverterVM.counter.value = currencyConverterVM.counter.value?.plus(1)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        currencyConverterVM.integerUserInput.observe(this, androidx.lifecycle.Observer {
            if(!binding.etUserInput.text.isNullOrEmpty()) {
                currencyConverterVM.counter.value = currencyConverterVM.counter.value?.plus(1)
            }
        })

        currencyConverterVM.counter.observe(this, androidx.lifecycle.Observer {
            if(!binding.etUserInput.text.isNullOrEmpty()) {
                 if(currencyConverterVM.spinnerInput.value.toString() == "rupees") {
                     currencyConverterVM.userInputInRupees.value = currencyConverterVM.integerUserInput.value?.toDouble()
                 }else if(currencyConverterVM.spinnerInput.value.toString() == "dollar") {
                     currencyConverterVM.userInputInRupees.value = currencyConverterVM.integerUserInput.value?.toDouble()?.times(74.71)
                 }else if(currencyConverterVM.spinnerInput.value.toString() == "pounds") {
                     currencyConverterVM.userInputInRupees.value = currencyConverterVM.integerUserInput.value?.toDouble()?.times(100.72)
                 }else if(currencyConverterVM.spinnerInput.value.toString() == "kuwaitiDinar") {
                     currencyConverterVM.userInputInRupees.value = currencyConverterVM.integerUserInput.value?.toDouble()?.times(247.01)
                 }
            }
        })

        currencyConverterVM.userInputInRupees.observe(this, androidx.lifecycle.Observer {
            currencyConverterVM.rupessTextView.value = it.toDouble()
            currencyConverterVM.dolloarTextView.value = it.toDouble()/74.71
            currencyConverterVM.poundsTextView.value = it.toDouble()/100.72
            currencyConverterVM.dinarTextView.value = it.toDouble()/247.01

        })
    }
}