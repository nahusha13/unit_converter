package com.example.unitconverter.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.unitconverter.R
import com.example.unitconverter.databinding.ActivityTemperatureConverterBinding
import com.example.unitconverter.viewmodel.TemperatureConverterViewModel

class TemperatureConverter : AppCompatActivity() {
    lateinit var binding:ActivityTemperatureConverterBinding
    lateinit var temperatureConverterVM:TemperatureConverterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_temperature_converter)
        temperatureConverterVM = ViewModelProvider(this).get(TemperatureConverterViewModel::class.java)
        binding.tempteratureVM = temperatureConverterVM
        binding.lifecycleOwner = this

        binding.spinnerUnitSelector.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var text = binding.spinnerUnitSelector.selectedItem.toString()
                temperatureConverterVM.spinnerInput.value = text

                temperatureConverterVM.counter.value = temperatureConverterVM.counter.value?.plus(1)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        temperatureConverterVM.integerUserInput.observe(this, Observer {
            if(!binding.etUserInput.text.isNullOrEmpty()) {
                temperatureConverterVM.counter.value = temperatureConverterVM.counter.value?.plus(1)
            }
        })

        temperatureConverterVM.counter.observe(this, Observer {
            if(!binding.etUserInput.text.isNullOrEmpty()) {
                if(temperatureConverterVM.spinnerInput.value.toString() == "celsius") {
                    temperatureConverterVM.userInputInCelsius.value = temperatureConverterVM.integerUserInput.value?.toDouble()
                }else if(temperatureConverterVM.spinnerInput.value.toString() =="Fahrenheit") {
                    temperatureConverterVM.userInputInCelsius.value = (temperatureConverterVM.integerUserInput.value?.toDouble()?.minus(32.0)?.times(5.0/9.0) )
                }
            }
        })

        temperatureConverterVM.userInputInCelsius.observe(this, Observer {
            temperatureConverterVM.celsiusTextView.value = it.toDouble()
            temperatureConverterVM.FahrenheitTextView.value = (it.toDouble().times(9.0/5.0)).plus(32.0)
        })
    }
}