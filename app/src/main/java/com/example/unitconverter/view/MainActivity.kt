package com.example.unitconverter.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.unitconverter.R
import com.example.unitconverter.databinding.ActivityMainBinding
import com.example.unitconverter.viewmodel.MainActivityViewModel

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    lateinit var mainViewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_UnitConverter)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        mainViewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)
        binding.mainVM = mainViewModel

        binding.cartViewLength.setOnClickListener {
            var intent = Intent(this, Converter::class.java)
            startActivity(intent)
        }

        binding.cardViewWeight.setOnClickListener {
            var intent = Intent(this,WeightConverter::class.java)
            startActivity(intent)
        }
        binding.cardViewCurrency.setOnClickListener {
            var intent = Intent(this,CurrencyConverter::class.java)
            startActivity(intent)
        }
        binding.cardViewDistance.setOnClickListener {
            var intent = Intent(this,DistanceConverter::class.java)
            startActivity(intent)
        }
        binding.carViewTemperature.setOnClickListener {
            var intent = Intent(this,TemperatureConverter::class.java)
            startActivity(intent)
        }
    }
}