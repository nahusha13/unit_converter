package com.example.unitconverter.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.unitconverter.R
import com.example.unitconverter.databinding.ActivityDistanceConverterBinding
import com.example.unitconverter.viewmodel.DistanceConverterViewModel

class DistanceConverter : AppCompatActivity() {
    lateinit var binding : ActivityDistanceConverterBinding
    lateinit var distanceConverterVM : DistanceConverterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_distance_converter)
        distanceConverterVM = ViewModelProvider(this).get(DistanceConverterViewModel::class.java)
        binding.distanceVM = distanceConverterVM
        binding.lifecycleOwner = this

        binding.spinnerUnitSelector.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var text = binding.spinnerUnitSelector.selectedItem.toString()
                distanceConverterVM.spinnerInput.value = text

                distanceConverterVM.counter.value = distanceConverterVM.counter.value?.plus(1)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
        distanceConverterVM.integerUserInput.observe(this, Observer {
            if(!binding.etUserInput.text.isNullOrEmpty()) {
                distanceConverterVM.counter.value = distanceConverterVM.counter.value?.plus(1)
            }
        })

        distanceConverterVM.counter.observe(this, Observer {
            if(!binding.etUserInput.text.isNullOrEmpty()) {
                if(distanceConverterVM.spinnerInput.value.toString() == "metre") {
                    distanceConverterVM.userInputInMetres.value = distanceConverterVM.integerUserInput.value?.toDouble()
                }else if(distanceConverterVM.spinnerInput.value.toString() == "kilometre") {
                    distanceConverterVM.userInputInMetres.value = distanceConverterVM.integerUserInput.value?.toDouble()?.times(1000.0)
                }else if(distanceConverterVM.spinnerInput.value.toString() == "miles") {
                    distanceConverterVM.userInputInMetres.value = distanceConverterVM.integerUserInput.value?.toDouble()?.times(1609.34)
                }
            }
        })

        distanceConverterVM.userInputInMetres.observe(this, Observer {
            distanceConverterVM.metreTextView.value = it.toDouble()
            distanceConverterVM.kilometreTextView.value = it.toDouble()/1000.00
            distanceConverterVM.mileTextView.value = it.toDouble()/1609.34
        })

    }
}