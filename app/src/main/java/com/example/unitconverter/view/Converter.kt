package com.example.unitconverter.view

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.view.get
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.unitconverter.R
import com.example.unitconverter.databinding.ActivityConverterBinding
import com.example.unitconverter.viewmodel.LengthConverterViewModel
import kotlin.math.pow

class Converter : AppCompatActivity() {
    lateinit var binding: ActivityConverterBinding
    lateinit var lenConveterVm:LengthConverterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_converter)
        lenConveterVm = ViewModelProvider(this).get(LengthConverterViewModel::class.java)
        binding.lenConverterVM = lenConveterVm
        binding.lifecycleOwner = this

        binding.spinnerUnitSelector.onItemSelectedListener = object:AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var text:String = binding.spinnerUnitSelector.selectedItem.toString()
                lenConveterVm.spinnerInput.value = text

                lenConveterVm.counter.value= lenConveterVm.counter.value?.plus(1)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }

        lenConveterVm.integerUserInput.observe(this, Observer {
            if(!binding.etUserInput.text.isNullOrEmpty()) {
                lenConveterVm.counter.value = lenConveterVm.counter.value?.plus(1)
            }
        })

        lenConveterVm.counter.observe(this, Observer {
            if (!binding.etUserInput.text.isNullOrEmpty()) {
                //            lenConveterVm.userInputInMM.value = lenConveterVm.integerUserInput.value?.toDouble()
//                Log.i("spinnerItem","${lenConveterVm.spinnerInput.value.toString()}")
                if(lenConveterVm.spinnerInput.value.toString() =="mm") {
                    lenConveterVm.userInputInMM.value = lenConveterVm.integerUserInput.value?.toDouble()
                }else if(lenConveterVm.spinnerInput.value.toString() =="cm") {
                    lenConveterVm.userInputInMM.value = lenConveterVm.integerUserInput.value?.toDouble()?.times(10)
                }else if(lenConveterVm.spinnerInput.value.toString() == "m") {
                    lenConveterVm.userInputInMM.value = lenConveterVm.integerUserInput.value?.toDouble()?.times(Math.pow(10.0,3.0))
                }else if(lenConveterVm.spinnerInput.value.toString() == "km") {
                    lenConveterVm.userInputInMM.value = lenConveterVm.integerUserInput.value?.toDouble()?.times(Math.pow(10.0,6.0))
                }
            }
        })

        lenConveterVm.userInputInMM.observe(this, Observer {
            lenConveterVm.mmTextView.value = it.toDouble()
            lenConveterVm.cmTextView.value =it.toDouble()/(10)
            lenConveterVm.mTextView.value = it.toDouble()/ (Math.pow(10.0, 3.0))
            lenConveterVm.kmTextView.value = it.toDouble()/(Math.pow(10.0, 6.0))
        })
    }
}